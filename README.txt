Schedule Mail
=============
https://www.drupal.org/sandbox/empesan/2621224

Description
===========
Adds a new function like drupal_mail to allow modules to schedule an e-mail.
Instead of sending it directly the mails are sent during cron runs.
It also allows the modules to delete a mail from the queue, if is the mail is 
no longer necessary.

This is an API module, doesn't do anything by itself.
